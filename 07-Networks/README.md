# شبکه

یه شبکه مجموعه‌ای منطقی از دستگاه‌ها است. و هر شبکه شامل دو بخش پیشوند شبکه (Network Prefix) و ماسک شبکه (Network Mask) است. درواقع ازطریق ماسک شبکه می‌تعیینیم که کدام بخش ماسک شبکه تعییننده‌ی شبکه است و کدام بخش‌اش تعییننده‌ی میزبان (Host) است.

![شبکه](assets/network.png)

**توجه.** در هر شبکه سوییچ، دستگاه‌ها باید دارای پیشوند شبکه یکسان ولی میزبان متفاوت باشند.

**مثال.**

![مثال شبکه](assets/network_example.png)

**توجه.** در هر شبکه دو آی‌پی رزرویده شده‌اند، یکی همان آی‌پی شبکه است و دومین آی‌پی برادکست (همه‌ی بیت‌های بخش میزبان شبکه یک) است.

![مثال شبکه](assets/network_example_2.png)
